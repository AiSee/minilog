package log

import (
	"bytes"
	"fmt"
	stdLog "log"
	"os"
	"regexp"
	"runtime"
	"time"
)

type message struct {
	text      string
	level     Level
	timestamp time.Time
}
type Level uint8
type logger struct{}

const (
	L_fatal Level = iota
	L_alert
	L_critical
	L_error
	L_warning
	L_notice
	L_info
	L_debug
	L_print
)

var maxConsoleLevel = L_print
var maxSyslogLevel = L_print
var maxFileLevel = L_print
var logFile string
var loggingDisabled = false
var myLogger *logger
var msgLevelCheck = regexp.MustCompile(`(?i)^([^\p{L}\p{M}\d]*` + // Любой регистр, любые не-буквы в начале
	`(fatal|alert|critical|crit|error|err|e|warning|warn|w|notice|info|debug)s?` + // Список уровней
	`[^\p{L}\p{M}\d_/-]+)`) // Хотя бы одна не-буква в конце
var lastMessage string
var isRepeated = false

func (l Level) String() string {
	switch l {
	default:
		fallthrough
	case L_fatal:
		return "Fatal"
	case L_alert:
		return "Alert"
	case L_critical:
		return "Critical"
	case L_error:
		return "Error"
	case L_warning:
		return "Warning"
	case L_notice:
		return "Notice"
	case L_info:
		return "Info"
	case L_debug:
		return "Debug"
	}
}

func getText(args []interface{}, pattern string, level Level, file string, line int) string {
	var msg string
	if pattern == "" {
		msg = fmt.Sprint(args...)
	} else {
		msg = fmt.Sprintf(pattern, args...)
	}
	if level == L_print {
		return fmt.Sprintf("%s\n", msg)
	}
	return fmt.Sprintf("[%s] %s:%d - %s\n", level, file, line, msg)
}

func output(elem message) {
	msg := elem.timestamp.Format("2006-01-02 15:04:05 ") + elem.text
	if msg != lastMessage {
		if isRepeated {
			msg = "\n" + msg
			isRepeated = false
		}
		lastMessage = msg
	} else {
		msg = "."
		isRepeated = true
	}

	if elem.level <= maxConsoleLevel {
		if elem.level <= L_warning {
			fmt.Fprint(os.Stderr, msg)
		} else {
			fmt.Fprint(os.Stdout, msg)
		}
	}
	if (logFile != "") && (elem.level <= maxFileLevel) {
		f, err := os.OpenFile(logFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			println(err.Error())
			// Просто больше не будем пытаться туда писать
			logFile = ""
		} else {
			if _, err = f.WriteString(msg); err != nil {
				println(err.Error())
				logFile = ""
			}
			f.Close()
		}
	}
}

func msg(pattern string, args []interface{}, level Level, depth int) {
	if (level > maxConsoleLevel && level > maxSyslogLevel && level > maxFileLevel) || loggingDisabled {
		return
	}
	_, file, line, _ := runtime.Caller(depth)
	short := file
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			short = file[i+1:]
			break
		}
	}
	text := getText(args, pattern, level, short, line)
	output(message{text, level, time.Now()})
}

func (t *logger) Write(buf []byte) (ln int, err error) {
	level := L_notice
	m := msgLevelCheck.FindSubmatch(buf)
	if len(m) == 3 {
		buf = bytes.Replace(buf, m[1], []byte(""), 1)
		letter := bytes.ToLower(m[2][0:1])
		switch letter[0] {
		case 'f':
			level = L_fatal
		case 'a':
			level = L_alert
		case 'c':
			level = L_critical
		case 'e':
			level = L_error
		case 'w':
			level = L_warning
		case 'n':
			level = L_notice
		case 'i':
			level = L_info
		case 'd':
			level = L_debug
		default:
		}
	}
	ln = len(buf)
	i := []interface{}{string(bytes.TrimSpace(buf))}
	msg("", i, level, 4)
	if level == L_fatal {
		os.Exit(-1)
	}
	return
}

func CaptureStdLog() {
	stdLog.SetPrefix("")
	stdLog.SetFlags(0)
	stdLog.SetOutput(myLogger)
}

func RestoreStdLog() {
	stdLog.SetPrefix("")
	stdLog.SetFlags(stdLog.LstdFlags)
	stdLog.SetOutput(os.Stderr)
}

func InitFile(file string, level Level) {
	logFile = file
	maxFileLevel = level
}

// maxLevel - Самый большой уровень логов, который будет записан
func SetConsoleLevel(level Level) {
	maxConsoleLevel = level
}

func init() {
	CaptureStdLog()
}
