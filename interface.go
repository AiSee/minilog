package log

import "os"

func Fatal(args ...interface{}) {
	msg("", args, L_fatal, 2)
	os.Exit(-1)
}

func Fatalf(pattern string, args ...interface{}) {
	msg(pattern, args, L_fatal, 2)
	os.Exit(-1)
}

func Alert(args ...interface{}) {
	msg("", args, L_alert, 2)
}

func Alertf(pattern string, args ...interface{}) {
	msg(pattern, args, L_alert, 2)
}

func Critical(args ...interface{}) {
	msg("", args, L_critical, 2)
}

func Criticalf(pattern string, args ...interface{}) {
	msg(pattern, args, L_critical, 2)
}

func Error(args ...interface{}) {
	msg("", args, L_error, 2)
}

func Errorf(pattern string, args ...interface{}) {
	msg(pattern, args, L_error, 2)
}

func Warning(args ...interface{}) {
	msg("", args, L_warning, 2)
}

func Warningf(pattern string, args ...interface{}) {
	msg(pattern, args, L_warning, 2)
}

func Notice(args ...interface{}) {
	msg("", args, L_notice, 2)
}

func Noticef(pattern string, args ...interface{}) {
	msg(pattern, args, L_notice, 2)
}

func Info(args ...interface{}) {
	msg("", args, L_info, 2)
}

func Infof(pattern string, args ...interface{}) {
	msg(pattern, args, L_info, 2)
}

func Debug(args ...interface{}) {
	msg("", args, L_debug, 2)
}

func Debugf(pattern string, args ...interface{}) {
	msg(pattern, args, L_debug, 2)
}

func Print(args ...interface{}) {
	msg("", args, L_print, 2)
}

func Printf(pattern string, args ...interface{}) {
	msg(pattern, args, L_print, 2)
}
